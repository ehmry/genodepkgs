
let Genode = env:DHALL_GENODE

let Prelude = Genode.Prelude

let Args = { config : Genode.Init.Type, rom : Genode.BootModules.Type } : Type

let RomEntry = Prelude.Map.Entry Text Genode.BootModules.ROM.Type

let addLine =
        λ(e : RomEntry)
      → λ(script : Text)
      → merge
          { RomText =
                λ(rom : Text)
              → ''
                ${script}
                echo ${Text/show rom} > ${Text/show e.mapKey}
                ''
          , RomPath =
                λ(rom : Text)
              → ''
                ${script}
                ln -s ${Text/show rom} ${Text/show e.mapKey}
                ''
          }
          e.mapValue

in    λ(args : Args)
    → λ(out : Text)
    → { config = Genode.Init.render args.config
      , script =
          Prelude.List.fold
            RomEntry
            args.rom
            Text
            addLine
            ''
            #!/bin/sh
            ln -s ${out}/config config
            ''
      }

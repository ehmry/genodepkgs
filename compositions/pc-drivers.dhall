
let Genode = env:DHALL_GENODE

let Prelude = Genode.Prelude

let XML = Prelude.XML

let Init = Genode.Init

let Child = Init.Child

let Resources = Init.Resources

let ServiceRoute = Init.ServiceRoute

let label =
        λ(label : Text)
      → { local = label, route = label } : Child.Attributes.Label

let relabel =
        λ(local : Text)
      → λ(route : Text)
      → { local = local, route = route } : Child.Attributes.Label

let drivers =
      Init::{
      , routes = [ ServiceRoute.parent "Timer" ]
      , services =
        [ ServiceRoute.child "Framebuffer" "dynamic"
        , ServiceRoute.child "Block" "dynamic"
        , ServiceRoute.child "Usb" "usb_drv"
        , ServiceRoute.child "Platform" "platform_drv"
        , ServiceRoute.child "Input" "event_filter"
        ]
      , children = toMap
          { rom_reporter =
              Child.flat
                Child.Attributes::{
                , binary = "rom_reporter"
                , config = Init.Config::{
                  , content =
                    [ XML.text
                        ''
                        <rom label="acpi"/>
                        <rom label="pci_devices"/>
                        <rom label="usb_devices"/>
                        ''
                    ]
                  }
                , reportRoms =
                  [ label "acpi", label "pci_devices", label "usb_devices" ]
                , routes =
                    let routeReportToParent =
                            λ(label : Text)
                          → ServiceRoute.parentLabel
                              "Report"
                              (Some label)
                              (Some label)

                    in  [ routeReportToParent "acpi"
                        , routeReportToParent "pci_devices"
                        , routeReportToParent "usb_devices"
                        ]
                }
          , acpi_drv =
              Child.flat
                Child.Attributes::{
                , binary = "acpi_drv"
                , priority = 1
                , resources = Resources::{
                  , caps = 350
                  , ram = Genode.units.MiB 4
                  }
                , romReports = [ label "acpi" ]
                , routes =
                  [ ServiceRoute.parent "IO_MEM"
                  , ServiceRoute.parentLabel
                      "Report"
                      (Some "smbios_table")
                      (Some "smbios_table")
                  ]
                }
          , platform_drv =
              Child.flat
                Child.Attributes::{
                , binary = "platform_drv"
                , resources = Resources::{
                  , caps = 400
                  , ram = Genode.units.MiB 4
                  , constrainPhys = True
                  }
                , provides = [ "Acpi", "Platform" ]
                , reportRoms = [ label "acpi" ]
                , romReports = [ relabel "pci" "pci_devices" ]
                , routes =
                  [ ServiceRoute.parent "IRQ"
                  , ServiceRoute.parent "IO_MEM"
                  , ServiceRoute.parent "IO_PORT"
                  , ServiceRoute.parentLabel
                      "ROM"
                      (Some "system")
                      (Some "system")
                  ]
                , config = Init.Config::{
                  , attributes = toMap { system = "yes" }
                  , content =
                    [ XML.text
                        ''
                        <report pci="yes"/>
                        <policy label_prefix="ps2_drv">
                          <device name="PS2"/>
                        </policy>
                        <policy label_prefix="dynamic -> vesa_fb_drv">
                          <pci class="VGA"/>
                        </policy>
                        <policy label_prefix="dynamic -> ahci_drv">
                          <pci class="AHCI"/>
                        </policy>
                        <policy label_prefix="dynamic -> nvme_drv">
                          <pci class="NVME"/>
                        </policy>
                        <policy label_prefix="usb_drv">
                          <pci class="USB"/>
                        </policy>
                        <policy label_prefix="dynamic -> intel_fb_drv">
                          <pci class="VGA"/>
                          <pci bus="0" device="0" function="0"/>
                          <pci class="ISABRIDGE"/>
                        </policy>
                        <policy label_suffix="-> wifi">
                          <pci class="WIFI"/>
                        </policy>
                        <policy label_suffix="-> nic">
                          <pci class="ETHERNET"/>
                        </policy>
                        <policy label_suffix="-> audio">
                          <pci class="AUDIO"/>
                          <pci class="HDAUDIO"/>
                        </policy>
                        <policy label="acpica"/>
                        ''
                    ]
                  }
                }
          , usb_drv =
              Child.flat
                Child.Attributes::{
                , binary = "usb_drv"
                , priority = 1
                , resources = Resources::{
                  , caps = 200
                  , ram = Genode.units.MiB 16
                  }
                , provides = [ "Input", "Usb" ]
                , reportRoms = [ relabel "config" "usb_drv.config" ]
                , romReports = [ relabel "devices" "usb_devices" ]
                , routes =
                  [ ServiceRoute.child "Platform" "platform_drv"
                  , ServiceRoute.parentLabel
                      "Report"
                      (Some "config")
                      (Some "usb_active_config")
                  ]
                }
          , ps2_drv =
              Child.flat
                Child.Attributes::{
                , binary = "ps2_drv"
                , provides = [ "Input" ]
                , routes = [ ServiceRoute.child "Platform" "platform_drv" ]
                }
          , event_filter =
              Child.flat
                Child.Attributes::{
                , binary = "event_filter"
                , priority = 1
                , resources = Resources::{ ram = Genode.units.MiB 2 }
                , provides = [ "Input" ]
                , routes =
                  [ ServiceRoute.parentLabel
                      "ROM"
                      (Some "config")
                      (Some "event_filter.config")
                  , ServiceRoute.childLabel
                      "Input"
                      "ps2_drv"
                      (Some "ps2")
                      (None Text)
                  , ServiceRoute.childLabel
                      "Input"
                      "usb_drv"
                      (Some "usb")
                      (None Text)
                  ]
                }
          , driver_manager =
              Child.flat
                Child.Attributes::{
                , binary = "driver_manager"
                , priority = 1
                , reportRoms =
                  [ label "ahci_ports"
                  , label "nvme_ns"
                  , label "pci_devices"
                  , label "usb_devices"
                  ]
                , romReports =
                  [ relabel "init.config" "dynamic.config"
                  , label "usb_drv.config"
                  ]
                , routes =
                  [ ServiceRoute.parentLabel
                      "Report"
                      (Some "block_devices")
                      (Some "block_devices")
                  , ServiceRoute.parentLabel
                      "ROM"
                      (Some "usb_policy")
                      (Some "usb_policy")
                  ]
                }
          , dynamic =
              Child.flat
                Child.Attributes::{
                , binary = "init"
                , priority = 1
                , resources = Resources::{
                  , caps = 1400
                  , ram = Genode.units.MiB 64
                  }
                , provides = [ "Framebuffer", "Block" ]
                , romReports = [ label "ahci_ports", label "nvme_ns" ]
                , reportRoms = [ relabel "config" "dynamic.config" ]
                , routes =
                  [ ServiceRoute.child "Platform" "platform_drv"
                  , ServiceRoute.child "Usb" "usb_drv"
                  , ServiceRoute.parent "Timer"
                  , ServiceRoute.parent "Report"
                  , ServiceRoute.parent "IO_MEM"
                  , ServiceRoute.parent "IO_PORT"
                  ]
                }
          }
      }

in  drivers

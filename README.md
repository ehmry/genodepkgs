```
``     ______                     __           __
`     / ____/__  ____  ____  ____/ /__  ____  / / ____ ______
     / / __/ _ \/ __ \/ __ \/ __  / _ \/ __ \/ / / __ `/ ___/
    / /_/ /  __/ / / / /_/ / /_/ /  __/ /_/ / , \ /_/ (__  )
`   \____/\___/_/ /_/\____/\__,_/\___/ .___/_/ \_\_, /____/
``                                  /_/        /____/
```

[Genodepkgs](https://git.sr.ht/~ehmry/genodepkgs) is nexus of
[Nix](https://nixos.org/nix/) expressions related to the
[Genode OS framework](https://genode.org).

[IRC](irc://freenode.net/#nixos-exotic)

[Mailing list](https://lists.sr.ht/~ehmry/genodepkgs)

[Sotest](https://opensource.sotest.io/userName%3Demery/1)

[Cachix](https://app.cachix.org/cache/genodepkgs)

[Hydra](https://hydra.nix-community.org/project/genodepkgs)

# Repository layout

 - Native packages are at [packages](./packages/default.nix)

 - Existing Nixpkgs packages are patched at [overlay](./overlay/default.nix)

 - Tests are at [tests](./tests/default.nix)

 - NixOS modules at [nixos-modules](./nixos-modules)

# Contributing

Patches can be submitted and issues reported via the
[mailing list](https://lists.sr.ht/~ehmry/genodepkgs). The mailing list is the
minimum viable mechanism for community development, and may be replaced later.

Issues may also be reported at the
[ticket tracker](https://todo.sr.ht/~ehmry/genodepkgs).

# Nix Flakes

This repository is structured as a Nix flake, which is an experimental extension
of Nix tooling. See https://www.tweag.io/posts/2020-05-25-flakes.html for an
introduction and tutorial.

## Build Caching

A binary cache is graciously provided by Cachix, the following link provides
instructions for enabling: https://app.cachix.org/cache/genodepkgs

## Building from flakes

```sh
$ git clone https://git.sr.ht/~ehmry/genodepkgs
$ cd genodepkgs
$ nix build -L .#checks.x86_64-linux.nova-x86

# View the log if the result is already cached:
$ nix log .#checks.x86_64-linux.nova-x86
```

# Packaging

Packaging is done using standard Nixpkgs methods, a `stdenv` is available for
cross-compilation. See [Solo5](./packages/solo5/default.nix) as an example.

## Cross-compiling Nixpkgs

Some existing packages from Nixpkgs can be built with little or no modification.
Nixpkgs is available at the path `.#legacyPackages.x86_64-linux-x86_64-genode`
(or `…-aarch64-genode`) and modifications to packages are made at an
[overlay](./overlay/default.nix). Some packages need patched, others require
additional features added to the Genode libc.

Packages that run on Linux as well as Genode should not be packaged here, those
should be added to Nixpkgs.

# Toolchain

The Genode Labs toolchain is available here only for Linux and in binary form.
However, an experimental LLVM cross-compiler is provided here. Bootstrapping GCC
has not been practical due to its recursive autotools build system.

The enter a shell with the toolchain:

```shell
nix shell .#legacyPackages.x86_64-linux-x86_64-genode.stdenv.cc

x86_64-unknown-genode-clang++ -v
```

# Tips and tricks

## How to add missing ports to a depot package?

The missing ports as indicated by a depot build failure must be added to
[targets.nix](./overlay/genode/targets.nix). For example:

```nix
{
  wifi_drv = { portInputs = [ dde_linux libc openssl ]; };
}
```

## How to add undefined ports?

Should a required port not be defined, it must be added to
[ports.nix](./overlay/genode/ports.nix). Initially it should be sufficient to
define the port as an empty attribute set, run the build again, and then add the
hash found in the successive build error.

To continue from the previous example:

```nix
{
  dde_linux = { };
}
```

And after the reattempting the build a fixed-output hash should be produced,
this must now be added to the port definitions.

```nix
{
  dde_linux = {
    hash = "sha256-3G5Mprv9ZBNhDJRuVF6Nrkp3KbFaN10IKFq0OdyAY9M="; };
}
```

Should the port preparation fail do to missing utilities, these must be added to
the port definition using a `nativeBuildInputs` attribute.

Note that the hash checked into `ports.nix` is an *output hash*. The port hash
within the Genode source tree is an *explicit input hash* and the output of
the port preparation is non-deterministic.


## Enter an environment compatible with the Genode Labs build scripts

```shell
nix dev-shell genodepkgs
cd $GENODE_DIR
make -C build/x86_64 run/bomb
```

# Glossary

Some of terms that follow are necessary for understanding packaging methodology,
Others are simply introduced as an aid to the reader:

 - *Upstream* - repositories packaged by expressions in this repository

 - *Downstream* - repositories depending on this repository

 - *OS ABI* - The binary interface to an operating system. In the case of Genode
   this does not include kernel interfaces, as system calls are abstracted via
   dynamically linked procedures.

 - `localSystem` - the CPU and OS ABI pair used to evaluate and compile
   packages. Genode cannot natively instantiate Nix derivations, therefore it
   cannot be a `localSystem` ABI.

 - `crossSystem` - the target CPU and OS ABI pair for packages. In the common
   case the ABI will be Genode. In the case of tooling it may be the same as
   `localSystem`. For software that runs bare-metal the CPU is significant but
   ABI is irrelevant.

 - `buildPlatform` - equivalent to `localSystem`.

 - `hostPlatform` - the platform on which a package will execute.

 - `targetPlatform` - the platform for which a package will produce artifacts.
   This is only significant for software such as compilers. The respective
   build, host, and target platform of a compiler that was built on a Solaris,
   executes on Genode, and produces only Haiku binaries would be Solaris,
   Genode, and Haiku.

 - *Flake* - a code repository containing a `flake.nix` file describing
   dependency repositories. The evaluation of flakes may only form ascyclic
   graphs. Repositories packaged here may themselves contain a `flake.nix`
   file that refers to this repository, but these repositories cannot be inputs
   to the Genodepkgs flake.

 - *Genode core* - Genode component of highest privilege. The core is located
   conceptually at the bottom and root of the component tree. Core might be
   directly above the kernel, or acting as the kernel. The role of core is a
   subset of the role of the kernel in a monolithic system.

 - *Lower* - lowering a subsystem is moving it closer to the Genode core
   and increasing privilege.

 - *Raise* - raising a subsystem is moving it further away from the Genode core
   and decreasing privilege.

 - *Package* - *undefined*

# Legal

This repository is published under MIT licensing consistent with Nixpkgs.

> Note: MIT license does not apply to the packages built by Nixpkgs,
> merely to the files in this repository (the Nix expressions, build
> scripts, NixOS modules, etc.). It also might not apply to patches
> included in Nixpkgs, which may be derivative works of the packages to
> which they apply. The aforementioned artifacts are all covered by the
> licenses of the respective packages.

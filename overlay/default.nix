{ flake }:
final: prev:
with prev;

let
  # Helper functions to override package dependant
  # on whether the host or target system is Genode.

  overrideHost = attrs: drv:
    if hostPlatform.isGenode then drv.override attrs else drv;

  overrideTarget = attrs: drv:
    if targetPlatform.isGenode then drv.override attrs else drv;

  overrideAttrsHost = f: drv:
    if hostPlatform.isGenode then drv.overrideAttrs f else drv;

  overrideAttrsTarget = f: drv:
    if targetPlatform.isGenode then drv.overrideAttrs f else drv;

  addPatches' = patches: attrs: { patches = attrs.patches or [ ] ++ patches; };

  addPatchesHost = ps: overrideAttrsHost (addPatches' ps);

  addPatchesTarget = ps: overrideAttrsTarget (addPatches' ps);

  autoreconfHost = overrideAttrsHost
    (attrs: { nativeBuildInputs = [ final.autoreconfHook ]; });

in {

  bash = overrideAttrsTarget (attrs: {
    configureFlags = attrs.configureFlags
      ++ [ "--without-bash-malloc" ]; # no sbrk please
    postPatch = "sed '/check_dev_tty/d' shell.c";
  }) bash;

  binutils-unwrapped = overrideAttrsTarget (attrs: {
    patches = attrs.patches ++ [ ./binutils/support-genode.patch ];
    nativeBuildInputs = [ final.updateAutotoolsGnuConfigScriptsHook ];
  }) binutils-unwrapped;

  cmake = overrideAttrsTarget (attrs: {
    postInstall = with stdenv; ''
      local MODULE="$out/share/cmake-${
        lib.versions.majorMinor attrs.version
      }/Modules/Platform/Genode.cmake"
      if [ -e "$MODULE" ]; then
          echo "Upstream provides $MODULE!"
          exit 1
      fi
      cp ${./cmake/Genode.cmake} $MODULE
    '';
  }) cmake;

  coreutils = overrideHost {
    gmp = null;
    libiconv = null;
  } (overrideAttrsHost (attrs: {
    configureFlags = [
      "--disable-acl"
      "--disable-largefile"
      "--disable-xattr"
      "--disable-libcap"
      "--disable-nls"
    ];
    LDFLAGS = [ "-Wl,--no-as-needed" ];
    # keep libposix NEEDED
  }) coreutils);

  gdb = addPatchesTarget [ ./gdb/genode.patch ] gdb;

  genodeLibcCross = callPackage ./libc { };

  genodePackages = import ../packages {
    inherit flake;
    pkgs = final;
  };
  libconfig =
    overrideAttrsHost (attrs: { configureFlags = [ "--disable-examples" ]; })
    libconfig;

  libcCrossChooser = name:
    if stdenv.targetPlatform.isGenode then
      targetPackages.genodeLibcCross
    else
      prev.libcCrossChooser name;

  libsodium = overrideAttrsHost (attrs: {
    patches = (attrs.patches or [ ]) ++ [ ./libsodium/genode.patch ];
  }) libsodium;

  libkrb5 = autoreconfHost libkrb5;

  libtool = overrideAttrsTarget (attrs: {
    nativeBuildInputs = with final;
      attrs.nativeBuildInputs ++ [ autoconf automake115x ];
    patches = ./libtool/genode.patch;
  }) libtool;

  libtoxcore = overrideHost {
    libopus = null;
    libvpx = null;
  } libtoxcore;

  llvmPackages_11 = callPackage ./llvm-11 ({
    inherit (stdenvAdapters) overrideCC;
    buildLlvmTools = buildPackages.llvmPackages_11.tools;
    targetLlvmLibraries = targetPackages.llvmPackages_11.libraries;
    inherit (final.genodePackages.genodeSources) genodeBase;
  } // stdenv.lib.optionalAttrs
    (stdenv.hostPlatform.isi686 && buildPackages.stdenv.cc.isGNU) {
      stdenv = gcc7Stdenv;
    });

  ncurses = addPatchesHost [ ./ncurses/genode.patch ] ncurses;

  openssl =
    overrideHost { static = true; } # shared library comes out stupid big
    (overrideAttrsHost (attrs: {
      outputs = [ "out" ]
        ++ builtins.filter (x: x != "bin" && x != "out") attrs.outputs;
      patches = attrs.patches or [ ] ++ [ ./openssl/genode.patch ];
      configureScript = {
        x86_64-genode = "./Configure Genode-x86_64";
      }.${stdenv.hostPlatform.system} or (throw
        "Not sure what configuration to use for ${stdenv.hostPlatform.config}");
      configureFlags = attrs.configureFlags ++ [ "no-devcryptoeng" ];
      postInstall =
        "rm $out/bin/c_rehash"; # eliminate the perl runtime dependency
    }) openssl);

  rsync = overrideHost {
    enableACLs = false;
    popt = null;
  } (overrideAttrsHost (attrs: { outputs = [ "out" "man" ]; }) rsync);

  solo5-tools = callPackage ./solo5-tools { };

  tor = overrideAttrsHost (attrs: {
    configureFlags = attrs.configureFlags or [ ]
      ++ [ "--disable-tool-name-check" ];
    patches = attrs.patches or [ ] ++ [ ./tor/genode.patch ];
    postPatch = null; # Avoid torsocks patching
  }) tor;

  zlib = overrideAttrsHost (attrs: {
    postInstall = attrs.postInstall or "" + ''
      pushd ''${!outputLib}/lib
      find . -type l -delete
      mv libz.so.* libz.so
      popd
    '';
  }) zlib;

}

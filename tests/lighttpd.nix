{
  name = "lighttpd";
  machine = { pkgs, ... }: {
    imports = [ ../nixos-modules/systemd.nix ];
    services.lighttpd = {
      enable = true;
    };
    systemd.services.lighttpd.genode.enable = true;
  };
}

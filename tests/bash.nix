{
  name = "bash";
  machine = { pkgs, ... }: {
    genode.init.children.bash = {
      configFile = pkgs.writeText "bash.child.dhall" ''
        ${
          ./bash.dhall
        } { bash = "${pkgs.bash}", coreutils = "${pkgs.coreutils}" }
      '';
      inputs = with pkgs.genodePackages; [
        pkgs.bash
        cached_fs_rom
        libc
        posix
        vfs
        vfs_pipe
      ];
    };
  };
  testScript = ''
    start_all()
    machine.wait_until_serial_output('child "bash" exited with exit value 0')
  '';
}

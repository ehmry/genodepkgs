let Genode = env:DHALL_GENODE

let Prelude = Genode.Prelude

let Init = Genode.Init

in  { Genode
    , Type =
        { children : Prelude.Map.Type Text Init.Child.Type
        , rom : Genode.BootModules.Type
        }
    , default.rom = [] : Genode.BootModules.Type
    , initToChildren =
        λ(init : Init.Type) →
          toMap
            { init =
                Init.toChild
                  init
                  Init.Attributes::{
                  , routes =
                    [ Init.ServiceRoute.parentLabel
                        "LOG"
                        (Some "SOTEST")
                        (Some "unlabeled")
                    , Init.ServiceRoute.parent "IO_MEM"
                    , Init.ServiceRoute.parent "IO_PORT"
                    , Init.ServiceRoute.parent "IRQ"
                    , Init.ServiceRoute.parent "VM"
                    ]
                  }
            }
    }

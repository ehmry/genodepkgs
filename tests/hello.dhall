let Genode =
        env:DHALL_GENODE
      ? https://git.sr.ht/~ehmry/dhall-genode/blob/master/package.dhall

let Init = Genode.Init

let Child = Init.Child

let Libc = Genode.Libc

in  Child.flat
      Child.Attributes::{
      , binary = "hello"
      , exitPropagate = True
      , resources = Genode.Init.Resources::{
        , caps = 500
        , ram = Genode.units.MiB 10
        }
      , config = Libc.toConfig Libc.default
      }

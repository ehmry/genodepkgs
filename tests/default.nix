{ flake, system, localSystem, crossSystem, pkgs }:

let
  lib = flake.lib.${system};
  nixpkgs = flake.legacyPackages.${system};
  legacyPackages = flake.legacyPackages.${system};

  testingPython = import ./lib/testing-python.nix;

  testSpecs = map (p: import p) [
    ./ahci.nix
    ./bash.nix
    ./hello.nix
    ./log.nix
    ./networking.nix
    ./vmm_x86.nix
    ./x86.nix
  ];

  cores = [
    /* {
         prefix = "hw-pc-";
         testingPython = testingPython {
           inherit flake system localSystem crossSystem pkgs;
           extraConfigurations = [ ../nixos-modules/base-hw-pc.nix ];
         };
         specs = [ "x86" "hw" ];
         platforms = [ "x86_64-genode" ];
       }
    */
    /* {
         prefix = "hw-virt_qemu-";
         testingPython = testingPython {
           inherit flake system localSystem crossSystem pkgs;
           extraConfigurations = [ ../nixos-modules/base-hw-virt_qemu.nix ];
         };
         specs = [ "aarch64" "hw" ];
         platforms = [ "aarch64-genode" ];
       }
    */
    {
      prefix = "nova-";
      testingPython = testingPython {
        inherit flake system localSystem crossSystem pkgs;
        extraConfigurations = [ ../nixos-modules/nova.nix ];
      };
      specs = [ "x86" "nova" ];
      platforms = [ "x86_64-genode" ];
    }
  ];

  cores' = builtins.filter (core:
    builtins.any (x: x == pkgs.stdenv.hostPlatform.system) core.platforms)
    cores;

  testList = let
    f = core: test:
      if (test.constraints or (_: true)) core.specs then {
        name = core.prefix + test.name;
        value = core.testingPython.makeTest test;
      } else
        null;

  in lib.lists.crossLists f [ cores' testSpecs ];

in builtins.listToAttrs (builtins.filter (_: _ != null) testList)

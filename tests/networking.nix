{
  name = "networking";
  nodes = {

    a = { pkgs, ... }: { imports = [ ../nixos-modules/hardware.nix ]; };

    b = { config, pkgs, lib, ... }: {
      imports = [ ../nixos-modules/hardware.nix ];
      networking.interfaces.eth1.genode.stack = null;
      genode.init.children.ping = {
        inputs = with pkgs.genodePackages; [ ping ];

        routeToNics = [ "eth1" ];

        configFile = let
          ipA = builtins.head config.networking.interfaces.eth1.ipv4.addresses;
          ipAddrB = with builtins;
            head (attrNames
              (lib.attrsets.filterAttrs (name: (any (host: host == "a")))
                config.networking.hosts));
        in pkgs.writeText "ping.dhall" ''
          let Genode = env:DHALL_GENODE

          let Init = Genode.Init

          let Child = Init.Child

          in  Child.flat
                Child.Attributes::{
                , binary = "ping"
                , resources = Init.Resources::{ ram = Genode.units.MiB 8 }
                , routes = [ Init.ServiceRoute.child "Nic" "eth1.driver" ]
                , config = Init.Config::{
                  , attributes = toMap
                      { interface = "${ipA.address}/${
                        toString ipA.prefixLength
                      }"
                      , dst_ip = "${ipAddrB}"
                      , period_sec = "1"
                      , count = "10"
                      , verbose = "yes"
                      }
                  }
                }
        '';
      };
    };

  };
  testScript = ''
    start_all()
    b.wait_until_serial_output('child "ping" exited with exit value 0')
  '';
}

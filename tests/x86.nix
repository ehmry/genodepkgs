{
  name = "x86";
  constraints = builtins.any (spec: spec == "x86");
  machine = { pkgs, ... }: {
    genode.init.subinits.test = {
      configFile = ./x86.dhall;
      coreROMs = [ "platform_info" ];
      inputs = with pkgs.genodePackages; [
        acpi_drv
        platform_drv
        report_rom
        test-signal
        test-pci
        test-rtc
      ];
    };
  };
  testScript = ''
    start_all()
    machine.wait_until_serial_output("child \"test\" exited with exit value 0")
  '';
}

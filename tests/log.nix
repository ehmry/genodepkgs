{
  name = "log";
  machine = { pkgs, ... }: {
    genode.init.children.log = {
      configFile = ./log.dhall;
      inputs = [ pkgs.genodePackages.test-log ];
    };
  };
  testScript = ''
    start_all()
    machine.wait_until_serial_output("Test done.")
  '';
}

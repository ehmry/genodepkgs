{
  name = "hello";
  machine = { pkgs, ... }:
    let
      hello = pkgs.stdenv.mkDerivation {
        name = "hello";
        dontUnpack = true;
        buildPhase = ''
          cat > hello.c << EOF
          #include <stdio.h>
          int main(int argc, char **argv) { printf("hello world!\n"); return 0; }
          EOF

          $CC hello.c -o hello
        '';
        installPhase = "install -Dt $out/bin hello";
      };
    in {
      genode.init.children.hello = {
        configFile = ./hello.dhall;
        inputs = [ hello pkgs.genodePackages.vfs.lib ];
        # TODO: libc.lib.so has only relative linking to vfs.lib.so
      };
    };
  testScript = ''
    start_all()
    machine.wait_until_serial_output("child \"init\" exited with exit value 0")
  '';
}

let Genode = env:DHALL_GENODE

let Init = Genode.Init

let Child = Init.Child

in  Child.flat
      Child.Attributes::{
      , binary = "test-vmm_x86"
      , exitPropagate = True
      , resources = Init.Resources::{ caps = 2048, ram = Genode.units.MiB 256 }
      , routes = [ Genode.Init.ServiceRoute.parent "VM" ]
      }

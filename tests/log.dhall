let Genode =
        env:DHALL_GENODE
      ? https://git.sr.ht/~ehmry/dhall-genode/blob/master/package.dhall

let Child = Genode.Init.Child

in  Child.flat
      Child.Attributes::{
      , binary = "test-log"
      , exitPropagate = True
      , resources = Genode.Init.Resources::{
        , caps = 500
        , ram = Genode.units.MiB 10
        }
      }

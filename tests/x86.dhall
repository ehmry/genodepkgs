let Genode = env:DHALL_GENODE

let XML = Genode.Prelude.XML

let Init = Genode.Init

let Child = Init.Child

let Resources = Init.Resources

let ServiceRoute = Init.ServiceRoute

let label = λ(_ : Text) → { local = _, route = _ } : Child.Attributes.Label

let pciInit =
      Init::{
      , verbose = True
      , routes = [ ServiceRoute.parent "Timer", ServiceRoute.parent "Rtc" ]
      , children = toMap
          { signal =
              Child.flat
                Child.Attributes::{
                , binary = "test-signal"
                , exitPropagate = True
                , priority = 5
                , resources = Init.Resources::{
                  , caps = 500
                  , ram = Genode.units.MiB 10
                  }
                }
          , rtc = Child.flat Child.Attributes::{ binary = "test-rtc" }
          , test-pci =
              Child.flat
                Child.Attributes::{
                , binary = "test-pci"
                , resources = Resources::{ ram = Genode.units.MiB 3 }
                }
          , acpi_drv =
              Child.flat
                Child.Attributes::{
                , binary = "acpi_drv"
                , resources = Resources::{
                  , caps = 400
                  , ram = Genode.units.MiB 4
                  , constrainPhys = True
                  }
                , romReports = [ label "acpi", label "smbios_table" ]
                , routes =
                  [ ServiceRoute.parent "IRQ"
                  , ServiceRoute.parent "IO_MEM"
                  , ServiceRoute.parent "IO_PORT"
                  ]
                }
          , platform_drv =
              Child.flat
                Child.Attributes::{
                , binary = "platform_drv"
                , resources = Resources::{
                  , caps = 800
                  , ram = Genode.units.MiB 4
                  , constrainPhys = True
                  }
                , reportRoms = [ label "acpi" ]
                , provides = [ "Platform" ]
                , routes =
                  [ ServiceRoute.parent "IRQ"
                  , ServiceRoute.parent "IO_MEM"
                  , ServiceRoute.parent "IO_PORT"
                  ]
                , config = Init.Config::{
                  , policies =
                    [ Init.Config.Policy::{
                      , service = "Platform"
                      , label = Init.LabelSelector.prefix "test-pci"
                      , content =
                        [ XML.leaf
                            { name = "pci"
                            , attributes = toMap { class = "ALL" }
                            }
                        ]
                      }
                    ]
                  }
                }
          }
      }

in  pciInit

let Genode = env:DHALL_GENODE

let Prelude = Genode.Prelude

let Libc = Genode.Libc

let VFS = Genode.VFS

let Init = Genode.Init

let Child = Init.Child

in  λ(params : { bash : Text, coreutils : Text }) →
      let init =
            Init::{
            , verbose = True
            , routes =
              [ Init.ServiceRoute.parent "Timer"
              , Init.ServiceRoute.parent "Rtc"
              ]
            , children = toMap
                { vfs =
                    Child.flat
                      Child.Attributes::{
                      , binary = "vfs"
                      , provides = [ "File_system" ]
                      , resources = Genode.Init.Resources::{
                        , caps = 256
                        , ram = Genode.units.MiB 8
                        }
                      , routes =
                          Prelude.List.map
                            Text
                            Init.ServiceRoute.Type
                            Init.ServiceRoute.parent
                            [ "File_system", "Rtc" ]
                      , config = Init.Config::{
                        , content =
                          [ VFS.vfs
                              [ VFS.dir
                                  "dev"
                                  [ VFS.dir "pipes" [ VFS.leaf "pipe" ]
                                  , VFS.leaf "log"
                                  , VFS.leaf "null"
                                  , VFS.leaf "rtc"
                                  , VFS.leaf "zero"
                                  ]
                              , VFS.dir
                                  "usr"
                                  [ VFS.dir
                                      "bin"
                                      [ VFS.symlink
                                          "env"
                                          "${params.coreutils}/bin/env"
                                      ]
                                  ]
                              , VFS.dir "ram" [ VFS.leaf "ram" ]
                              , VFS.dir
                                  "nix"
                                  [ VFS.dir
                                      "store"
                                      [ VFS.fs VFS.FS::{ label = "nix-store" } ]
                                  ]
                              , VFS.inline
                                  "script.sh"
                                  ''
                                  bash --version
                                  bash -c "bash --version"
                                  ''
                              ]
                          ]
                        , policies =
                          [ Init.Config.Policy::{
                            , service = "File_system"
                            , label = Init.LabelSelector.prefix "shell"
                            , attributes = toMap
                                { root = "/", writeable = "yes" }
                            }
                          , Init.Config.Policy::{
                            , service = "File_system"
                            , label = Init.LabelSelector.prefix "vfs_rom"
                            , attributes = toMap { root = "/" }
                            }
                          ]
                        }
                      }
                , vfs_rom =
                    Child.flat
                      Child.Attributes::{
                      , binary = "cached_fs_rom"
                      , provides = [ "ROM" ]
                      , resources = Init.Resources::{
                        , caps = 256
                        , ram = Genode.units.MiB 32
                        }
                      , config = Init.Config::{
                        , policies =
                          [ Init.Config.Policy::{
                            , service = "ROM"
                            , label = Init.LabelSelector.prefix "shell"
                            }
                          ]
                        }
                      }
                , shell =
                    Child.flat
                      Child.Attributes::{
                      , binary = "${params.bash}/bin/bash"
                      , exitPropagate = True
                      , resources = Genode.Init.Resources::{
                        , caps = 256
                        , ram = Genode.units.MiB 8
                        }
                      , routes =
                          Prelude.List.map
                            Text
                            Init.ServiceRoute.Type
                            ( λ(label : Text) →
                                Init.ServiceRoute.parentLabel
                                  "ROM"
                                  (Some label)
                                  (Some label)
                            )
                            [ "libc.lib.so"
                            , "libm.lib.so"
                            , "posix.lib.so"
                            , "vfs.lib.so"
                            ]
                      , config =
                          ( Libc.toConfig
                              Libc::{
                              , pipe = Some "/dev/pipe"
                              , rtc = Some "/dev/rtc"
                              , vfs = [ VFS.fs VFS.FS::{ label = "root" } ]
                              , env = toMap
                                  { TERM = "screen"
                                  , PATH =
                                      "${params.coreutils}/bin:${params.bash}/bin"
                                  }
                              , args = [ "bash", "/script.sh" ]
                              }
                          )
                        with attributes = toMap { ld_verbose = "true" }
                      }
                }
            }

      in  Init.toChild init Init.Attributes::{=}

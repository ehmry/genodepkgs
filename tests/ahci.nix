{
  name = "ahci";
  machine = { pkgs, ... }: {
    genode.boot.storeBackend = "ahci";
    genode.init.children.hello = {
      inputs = [ pkgs.hello pkgs.genodePackages.vfs.lib ];
      configFile = pkgs.writeText "ahci-hello.child.dhall" ''
        let Genode = env:DHALL_GENODE

        let Init = Genode.Init

        let Child = Init.Child

        let Libc = Genode.Libc

        in  Child.flat
              Child.Attributes::{
              , binary = "hello"
              , exitPropagate = True
              , resources = Genode.Init.Resources::{
                , caps = 500
                , ram = Genode.units.MiB 10
                }
              , config =
                  Libc.toConfig
                    Libc::{ args = [ "hello", "-g", "Hello from AHCI test" ] }
              }
      '';
    };
  };
  testScript = ''
    start_all()
    machine.wait_until_serial_output("Hello from AHCI test")
  '';
}

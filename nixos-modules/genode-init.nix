{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.genode.init;
  coreROMs = mkOption {
    type = with types; listOf str;
    default = [ ];
    description = ''
      List of label suffixes that when matched against
      ROM requests shall be forwared to the core.
    '';
    example = [ "platform_info" ];
  };
  inputs = mkOption {
    description = "List of packages to build a ROM store with.";
    default = [ ];
    type = types.listOf types.package;
  };
in {

  options.genode.init = {

    verbose = mkEnableOption "verbose logging";

    configFile = mkOption {
      description = ''
        Dhall configuration of this init instance after children have been merged.
      '';
      type = types.path;
    };

    baseConfig = mkOption {
      description =
        "Dhall configuration of this init instance before merging children.";
      type = types.str;
      default = ''
        let Genode = env:DHALL_GENODE

        in  Genode.Init::{
            , routes =
              [ Genode.Init.ServiceRoute.parent "File_system"
              , Genode.Init.ServiceRoute.parent "IO_MEM"
              , Genode.Init.ServiceRoute.parent "IO_PORT"
              , Genode.Init.ServiceRoute.parent "IRQ"
              , Genode.Init.ServiceRoute.parent "Rtc"
              , Genode.Init.ServiceRoute.parent "Terminal"
              , Genode.Init.ServiceRoute.parent "Timer"
              ]
            }
      '';
    };

    children = mkOption {
      default = { };
      type = with types;
        attrsOf (submodule {
          options = {
            inherit coreROMs inputs;

            routeToNics = lib.mkOption {
              type = with types; listOf str;
              default = [ ];
              example = [ "eth0" ];
              description = ''
                Grant access to these Nic interfaces.
              '';
            };

            fsPersistence = lib.mkOption {
              type = types.bool;
              default = false;
              description = ''
                Whether this child will have access to mutable and persistent storage.
                This space is shared among all components for which this option is available
                and UNIX permission bits are not honored.
              '';
            };

            configFile = mkOption {
              type = types.path;
              description = ''
                Dhall configuration of child.
                See https://git.sr.ht/~ehmry/dhall-genode/tree/master/Init/Child/Type
              '';
            };
          };
        });
    };

    subinits =
      # Subinits are just a different kind of children.
      # Eventually this will be nested "genode.init" instances.
      mkOption {
        default = { };
        type = with types;
          attrsOf (submodule {
            options = {
              inherit coreROMs inputs;
              configFile = mkOption {
                type = types.path;
                description = ''
                  Dhall configuration of child init.
                  See https://git.sr.ht/~ehmry/dhall-genode/tree/master/Init/Type
                '';
              };
            };
          });
      };

  };

  config.genode.init = {

    # TODO: convert the subinits to children

    children = mapAttrs (name: value: {
      inherit (value) inputs;
      configFile = pkgs.writeText "${name}.child.dhall" ''
        let Genode = env:DHALL_GENODE

        in  Genode.Init.toChild ${value.configFile} Genode.Init.Attributes.default
      '';
    }) cfg.subinits;

    configFile = let

      children =
        lib.mapAttrsToList (name: value: ", `${name}` = ${value.configFile}")
        cfg.children;

    in pkgs.writeText "init.dhall" ''
      let Genode = env:DHALL_GENODE
      let baseConfig = ${cfg.baseConfig}

      in baseConfig // {
        , verbose = ${if config.genode.init.verbose then "True" else "False"}
        , children = baseConfig.children # toMap {${toString children} }
      }
    '';

  };

}

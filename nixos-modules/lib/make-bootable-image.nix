# Builds a compressed EFI System Partition image
{ config, pkgs, espImage, storeFsImage }:

pkgs.stdenv.mkDerivation {
  name = "boot.qcow2";

  nativeBuildInputs = with pkgs.buildPackages; [
    buildPackages.qemu_test
    utillinux
    zstd
  ];

  buildCommand = ''
    img=./temp.raw
    blockSize=512
    sectorSize=$(( $blockSize * 1 ))
    imgBytes=0

    espSectorOffset=2048
    esbByteOffset=$(( $espSectorOffset * $sectorSize ))

    # Pad the front of the image
    echo "Pad front of image with " $esbByteOffset " bytes"
    truncate --size=$esbByteOffset $img

    # Concatentenate the ESP
    echo "Concatenate ESP ${espImage}"
    zstdcat ${espImage} >> $img

    imgBytes=$(stat --format=%s $img)
    echo "Image is $(( $imgBytes >> 20 )) MiB with ESP partition"

    nixSectorOffset=$(( ($imgBytes + $sectorSize - 1) / $sectorSize ))
    nixByteOffset=$(( $nixSectorOffset * $sectorSize ))

    # Pad the ESP
    echo "Pad end of ESP with " $(( $nixByteOffset - $imgBytes )) " bytes"
    truncate --size=$nixByteOffset $img

    # Concatenate the nix partition
    echo "Concatenate store ${storeFsImage}"
    zstdcat ${storeFsImage} >> $img

    imgBytes=$(stat --format=%s $img)
    echo "Image is $(( $imgBytes >> 20 )) MiB with store partition"

    endSectorOffset=$(( ($(stat --format=%s $img) + $sectorSize + 1) / $sectorSize ))
    endByteOffset=$(( $endSectorOffset * $sectorSize ))

    # Pad the end of the image
    echo "Pad end of store with $(( $endByteOffset - $imgBytes )) bytes"
    truncate --size=$endByteOffset $img

    imgBytes=$(stat --format=%s $img)
    echo "Image is $(( $imgBytes >> 20 )) MiB with final padding"

    efiUuid=C12A7328-F81F-11D2-BA4B-00A0C93EC93B

    # Create the partition table
    sfdisk $img <<EOF
      label: gpt
      label-id: 44444444-4444-4444-8888-888888888888
      start=$nixSectorOffset, type=${config.genode.boot.storePartUuid}
      start=$espSectorOffset, type=$efiUuid
    EOF

    qemu-img convert -f raw -O qcow2 $img $out
  '';
}

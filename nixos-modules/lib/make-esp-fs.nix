# Builds a compressed EFI System Partition image
{ config, pkgs }:

let
  grub' = pkgs.buildPackages.grub2_efi;

  # Name used by UEFI for architectures.
  targetArch = if pkgs.stdenv.isi686 || config.boot.loader.grub.forcei686 then
    "ia32"
  else if pkgs.stdenv.isx86_64 then
    "x64"
  else if pkgs.stdenv.isAarch64 then
    "aa64"
  else
    throw "Unsupported architecture";

in pkgs.stdenv.mkDerivation {
  name = "esp.img.zst";

  nativeBuildInputs = with pkgs.buildPackages; [ grub' dosfstools mtools zstd ];

  MODULES = [
    "configfile"
    "efi_gop"
    "efi_uga"
    "ext2"
    "gzio"
    "multiboot"
    "multiboot2"
    "normal"
    "part_gpt"
    "search_fs_uuid"
  ];

  buildCommand = ''
    img=tmp.raw
    bootdir=EFI/boot/
    mkdir -p $bootdir

    cat <<EOF > embedded.cfg
    insmod configfile
    insmod efi_gop
    insmod efi_uga
    insmod ext2
    insmod normal
    insmod part_gpt
    insmod search_fs_uuid
    search.fs_uuid ${config.genode.boot.storeFsUuid} root
    set prefix=($root)/boot/grub
    configfile /boot/grub/grub.cfg
    EOF

    grub-script-check embedded.cfg

    ${grub'}/bin/grub-mkimage \
      --config=embedded.cfg \
      --output=$bootdir/boot${targetArch}.efi \
      --prefix=/boot/grub \
      --format=${grub'.grubTarget} \
      $MODULES

    # Make the ESP image twice as large as necessary
    imageBytes=$(du --summarize --block-size=4096 --total $bootdir | tail -1 | awk '{ print int($1 * 8192) }')

    truncate --size=$imageBytes $img
    mkfs.vfat -n EFIBOOT --invariant $img
    mcopy -sv -i $img EFI ::
    fsck.vfat -nv $img

    zstd --verbose --no-progress ./$img -o $out
  '';
}

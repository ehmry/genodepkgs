let Genode = env:DHALL_GENODE

let Prelude = Genode.Prelude

let XML = Prelude.XML

let Init = Genode.Init

let Child = Init.Child

let TextMapType = Prelude.Map.Type Text

let ChildMapType = TextMapType Child.Type

let Manifest/Type = TextMapType (TextMapType Text)

in  λ ( params
      : { extraCoreChildren : ChildMapType
        , subinit : Init.Type
        , storeSize : Natural
        , storeRomPolicies : Prelude.Map.Type Text Text
        , routes : List Init.ServiceRoute.Type
        , bootManifest : Manifest/Type
        }
      ) →
      Genode.Boot::{
      , config = Init::{
        , routes = params.routes
        , children =
            let child = Prelude.Map.keyValue Child.Type

            in    [ child
                      "timer"
                      ( Child.flat
                          Child.Attributes::{
                          , binary = "timer_drv"
                          , config = Init.Config::{
                            , policies =
                              [ Init.Config.Policy::{
                                , service = "Timer"
                                , label = Init.LabelSelector.none
                                }
                              ]
                            }
                          }
                      )
                  , child
                      "rtc"
                      ( Child.flat
                          Child.Attributes::{
                          , binary = "rtc_drv"
                          , routes = [ Init.ServiceRoute.parent "IO_PORT" ]
                          , config = Init.Config::{
                            , policies =
                              [ Init.Config.Policy::{
                                , service = "Rtc"
                                , label = Init.LabelSelector.none
                                }
                              ]
                            }
                          }
                      )
                  , child
                      "jitter_sponge"
                      ( Child.flat
                          Child.Attributes::{
                          , binary = "jitter_sponge"
                          , config = Init.Config::{
                            , policies =
                              [ Init.Config.Policy::{
                                , service = "Terminal"
                                , label = Init.LabelSelector.suffix "entropy"
                                }
                              ]
                            }
                          }
                      )
                  , child
                      "store_rom"
                      ( Child.flat
                          Child.Attributes::{
                          , binary = "cached_fs_rom"
                          , resources = Init.Resources::{
                            , ram = params.storeSize + Genode.units.MiB 1
                            }
                          , config = Init.Config::{
                            , policies =
                                  [ Init.Config.Policy::{
                                    , service = "ROM"
                                    , label =
                                        Init.LabelSelector.prefix
                                          "nixos -> /nix/store"
                                    }
                                  ]
                                # ( let Entry = Prelude.Map.Entry Text Text

                                    in  Prelude.List.concatMap
                                          Entry
                                          Init.Config.Policy.Type
                                          ( λ(e : Entry) →
                                              [ Init.Config.Policy::{
                                                , service = "ROM"
                                                , label =
                                                    Init.LabelSelector.prefix
                                                      "nixos -> ${e.mapKey}"
                                                , attributes = toMap
                                                    { directory =
                                                        "${e.mapValue}/bin"
                                                    }
                                                }
                                              , Init.Config.Policy::{
                                                , service = "ROM"
                                                , label =
                                                    Init.LabelSelector.Type.Partial
                                                      { prefix = Some
                                                          "nixos -> ${e.mapKey}"
                                                      , suffix = Some ".so"
                                                      }
                                                , attributes = toMap
                                                    { directory =
                                                        "${e.mapValue}/lib"
                                                    }
                                                }
                                              ]
                                          )
                                          params.storeRomPolicies
                                  )
                            }
                          }
                      )
                  ]
                # params.extraCoreChildren
                # [ child
                      "nixos"
                      ( Init.toChild
                          params.subinit
                          Init.Attributes::{
                          , exitPropagate = True
                          , resources = Init.Resources::{
                            , ram = Genode.units.MiB 4
                            }
                          , routes =
                              let parentROMs =
                                    Prelude.List.concatMap
                                      Text
                                      Init.ServiceRoute.Type
                                      ( λ(suffix : Text) →
                                          Prelude.List.map
                                            Text
                                            Init.ServiceRoute.Type
                                            ( λ(prefix : Text) →
                                                { service =
                                                  { name = "ROM"
                                                  , label =
                                                      Init.LabelSelector.Type.Partial
                                                        { prefix = Some prefix
                                                        , suffix = Some suffix
                                                        }
                                                  }
                                                , route =
                                                    Init.Route.parent
                                                      (Some suffix)
                                                }
                                            )
                                            ( Prelude.Map.keys
                                                Text
                                                Init.Child.Type
                                                params.subinit.children
                                            )
                                      )

                              in    parentROMs
                                      [ "ld.lib.so", "vfs.lib.so", "init" ]
                                  # [ Init.ServiceRoute.parent "IO_MEM"
                                    , Init.ServiceRoute.parent "IO_PORT"
                                    , Init.ServiceRoute.parent "IRQ"
                                    , Init.ServiceRoute.parent "VM"
                                    , Init.ServiceRoute.child "Timer" "timer"
                                    , Init.ServiceRoute.child "Rtc" "rtc"
                                    ]
                          }
                      )
                  ]
        }
      , rom =
          Genode.BootModules.toRomPaths
            ( Prelude.List.concat
                (Prelude.Map.Entry Text Text)
                ( Prelude.Map.values
                    Text
                    (Prelude.Map.Type Text Text)
                    params.bootManifest
                )
            )
      }

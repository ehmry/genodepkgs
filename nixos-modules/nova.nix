{ config, pkgs, lib, ... }:

with lib;
let
  localPackages = pkgs.buildPackages;
  utils = import ../lib {
    inherit (config.nixpkgs) system localSystem crossSystem;
    inherit pkgs;
  };

  bootDir = pkgs.runCommand "${config.system.name}-bootdir" { } ''
    mkdir $out
    gz() {
      gzip --keep --to-stdout "$1" > "$2"
    }
    gz ${pkgs.genodePackages.genodeSources}/tool/boot/bender $out/bender.gz
    gz ${pkgs.genodePackages.NOVA}/hypervisor-x86_64 $out/hypervisor.gz
    gz ${config.genode.boot.image}/image.elf $out/image.elf.gz
  '';

in {
  genode.core = {
    prefix = "nova-";
    supportedSystems = [ "x86_64-genode" ];
    basePackages = with pkgs.genodePackages; [ base-nova rtc_drv ];
  };

  genode.boot = {
    image =
      utils.novaImage config.system.name { } config.genode.boot.configFile;
  };

  genode.boot.storePaths =
    lib.optional (config.genode.boot.storeBackend != "tarball") bootDir;

  virtualisation.qemu.options =
    lib.optionals (!config.virtualisation.useBootLoader) [
      "-kernel '${pkgs.genodePackages.bender}/share/bender/bender'"
      "-initrd '${pkgs.genodePackages.NOVA}/hypervisor-x86_64 arg=iommu logmem novpid serial,${config.genode.boot.image}/image.elf'"
    ];

  virtualisation.qemu.kernel = "${pkgs.genodePackages.bender}/share/bender/bender";

  virtualisation.qemu.initrd = "${pkgs.genodePackages.NOVA}/hypervisor-x86_64";

  virtualisation.qemu.cmdline = "arg=iommu logmem novpid serial,${config.genode.boot.image}/image.elf";

  boot.loader.grub.extraEntries = ''
    menuentry 'Genode on NOVA' {
      insmod multiboot2
      insmod gzio
      multiboot2 ${bootDir}/bender.gz serial_fallback
      module2 ${bootDir}/hypervisor.gz hypervisor iommu logmem novga novpid serial
      module2 ${bootDir}/image.elf.gz image.elf
    }
  '';

}

let Genode = env:DHALL_GENODE

let Prelude = Genode.Prelude

let XML = Prelude.XML

let Libc = Genode.Libc

let VFS = Genode.VFS

let Init = Genode.Init

let Child = Init.Child

let parentRoutes =
      Prelude.List.map Text Init.ServiceRoute.Type Init.ServiceRoute.parent

in  λ ( params
      : { args : List Text
        , binary : Text
        , coreutils : Text
        , extraVfs : List XML.Type
        , fsPersistence : Bool
        , interface : Optional Text
        , ramQuotaMiB : Natural
        }
      ) →
      let socketsVfs =
            merge
              { Some =
                  λ(interface : Text) →
                    [ VFS.dir
                        "sockets"
                        [ VFS.fs VFS.FS::{ label = "${interface}.sockets" } ]
                    ]
              , None = [] : List XML.Type
              }
              params.interface

      let init =
            Init::{
            , routes =
                parentRoutes [ "File_system", "Rtc", "Terminal", "Timer" ]
            , children =
                  toMap
                    { vfs =
                        Child.flat
                          Child.Attributes::{
                          , binary = "vfs"
                          , exitPropagate = True
                          , resources = Genode.Init.Resources::{
                            , caps = 256
                            , ram = Genode.units.MiB 8
                            }
                          , config = Init.Config::{
                            , content =
                              [ VFS.vfs
                                  (   [ VFS.dir
                                          "dev"
                                          (   [ VFS.dir
                                                  "pipes"
                                                  [ VFS.leaf "pipe" ]
                                              , VFS.leaf "log"
                                              , VFS.leaf "null"
                                              , VFS.leafAttrs
                                                  "terminal"
                                                  ( toMap
                                                      { name = "entropy"
                                                      , label = "entropy"
                                                      }
                                                  )
                                              , VFS.leaf "rtc"
                                              , VFS.leaf "zero"
                                              ]
                                            # socketsVfs
                                          )
                                      , VFS.dir
                                          "usr"
                                          [ VFS.dir
                                              "bin"
                                              [ VFS.symlink
                                                  "env"
                                                  "${params.coreutils}/bin/env"
                                              ]
                                          ]
                                      , VFS.dir "tmp" [ VFS.leaf "ram" ]
                                      , VFS.dir
                                          "nix"
                                          [ VFS.dir
                                              "store"
                                              [ VFS.fs
                                                  VFS.FS::{
                                                  , label = "nix-store"
                                                  , writeable = "no"
                                                  }
                                              ]
                                          ]
                                      ]
                                    # ( if    params.fsPersistence
                                        then  [ VFS.fs
                                                  VFS.FS::{
                                                  , label = "peristence"
                                                  , writeable = "yes"
                                                  }
                                              ]
                                        else  [ VFS.leaf "ram" ]
                                      )
                                    # params.extraVfs
                                  )
                              ]
                            , policies =
                              [ Init.Config.Policy::{
                                , service = "File_system"
                                , label = Init.LabelSelector.prefix "ExecStart"
                                , attributes = toMap
                                    { root = "/", writeable = "yes" }
                                }
                              , Init.Config.Policy::{
                                , service = "File_system"
                                , label = Init.LabelSelector.prefix "vfs_rom"
                                , attributes = toMap
                                    { root = "/", writeable = "no" }
                                }
                              ]
                            }
                          }
                    , vfs_rom =
                        Child.flat
                          Child.Attributes::{
                          , binary = "cached_fs_rom"
                          , resources = Genode.Init.Resources::{
                            , ram = Genode.units.MiB 32
                            }
                          , config = Init.Config::{
                            , policies =
                              [ Init.Config.Policy::{
                                , service = "ROM"
                                , label = Init.LabelSelector.prefix "ExecStart"
                                }
                              ]
                            }
                          }
                    }
                # [ { mapKey = "ExecStart"
                    , mapValue =
                        Child.flat
                          Child.Attributes::{
                          , binary = params.binary
                          , exitPropagate = True
                          , resources = Genode.Init.Resources::{
                            , caps = 256
                            , ram = Genode.units.MiB params.ramQuotaMiB
                            }
                          , config =
                              ( Libc.toConfig
                                  Libc::{
                                  , stdin = Some "/dev/null"
                                  , stdout = Some "/dev/log"
                                  , stderr = Some "/dev/log"
                                  , pipe = Some "/dev/pipes"
                                  , rng = Some "/dev/entropy"
                                  , rtc = Some "/dev/rtc"
                                  , socket = Some "/dev/sockets"
                                  , vfs = [ VFS.leaf "fs" ]
                                  , args = [ params.binary ] # params.args
                                  }
                              )
                            with attributes = toMap { ld_verbose = "yes" }
                          , routes =
                              Prelude.List.map
                                Text
                                Init.ServiceRoute.Type
                                ( λ(label : Text) →
                                    Init.ServiceRoute.parentLabel
                                      "ROM"
                                      (Some label)
                                      (Some label)
                                )
                                [ "libc.lib.so"
                                , "libm.lib.so"
                                , "posix.lib.so"
                                , "vfs.lib.so"
                                ]
                          }
                    }
                  ]
            }

      in  Init.toChild init Init.Attributes::{=}

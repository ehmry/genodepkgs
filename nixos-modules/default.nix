{ flake }:

{

  x86_64 = {
    imports = [
      ./genode-core.nix
      ./genode-init.nix
      ./hardware.nix
      ./qemu-vm.nix
      ./systemd.nix
    ];
    nixpkgs = rec {
      localSystem = "x86_64-linux";
      crossSystem = "x86_64-genode";
      system = localSystem + "-" + crossSystem;
      pkgs = flake.legacyPackages.${system};
    };
  };

  nova = import ./nova.nix;

}

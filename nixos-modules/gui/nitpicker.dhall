let Genode = env:DHALL_GENODE

let XML = Genode.Prelude.XML

let Init = Genode.Init

in  λ(params : { policies : List Init.Config.Policy.Type }) →
      Init.Child.flat
        Init.Child.Attributes::{
        , binary = "nitpicker"
        , resources = Init.Resources::{ ram = Genode.units.MiB 4 }
        , config = Init.Config::{
          , attributes = toMap
              { note = "Nitpicker config is extremely fragile!" }
          , content =
            [ XML.leaf { name = "capture", attributes = XML.emptyAttributes }
            , XML.leaf
                { name = "domain"
                , attributes = toMap
                    { name = "default"
                    , layer = "1"
                    , content = "client"
                    , label = "no"
                    }
                }
            , XML.leaf
                { name = "background"
                , attributes = toMap { color = "#000000" }
                }
            ]
          , policies =
                params.policies
              # [ Init.Config.Policy::{
                  , service = "Capture"
                  , label = Init.LabelSelector.prefix "fb_drv"
                  }
                ]
          , defaultPolicy = Some Init.Config.DefaultPolicy::{
            , attributes = toMap { domain = "default" }
            }
          }
        }

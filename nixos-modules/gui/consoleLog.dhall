let Genode = env:DHALL_GENODE

let Prelude = Genode.Prelude

let VFS = Genode.VFS

let XML = Prelude.XML

let Init = Genode.Init

let Child = Init.Child

let Resources = Init.Resources

let ServiceRoute = Init.ServiceRoute

let routeLogRom =
      λ(label : Text) → ServiceRoute.parentLabel "ROM" (Some "log") (Some label)

in  λ(params : { fontFile : Text }) →
      Init.toChild
        Init::{
        , verbose = True
        , routes =
          [ Init.ServiceRoute.parent "Gui", Init.ServiceRoute.parent "Timer" ]
        , children = toMap
            { gui_fb =
                Child.flat
                  Child.Attributes::{
                  , binary = "gui_fb"
                  , exitPropagate = True
                  , resources = Resources::{ ram = Genode.units.MiB 8 }
                  , config = Init.Config::{
                    , attributes = toMap
                        { origin = "top_right"
                        , xpos = "0"
                        , ypos = "0"
                        , initial_width = "1024"
                        , initial_height = "768"
                        }
                    , policies =
                      [ Init.Config.Policy::{
                        , service = "Framebuffer"
                        , label = Init.LabelSelector.prefix "terminal"
                        }
                      , Init.Config.Policy::{
                        , service = "Input"
                        , label = Init.LabelSelector.prefix "terminal"
                        }
                      ]
                    }
                  }
            , terminal =
                Child.flat
                  Child.Attributes::{
                  , binary = "terminal"
                  , exitPropagate = True
                  , resources = Resources::{
                    , caps = 256
                    , ram = Genode.units.MiB 4
                    }
                  , routes =
                    [ ServiceRoute.child "Framebuffer" "gui_fb"
                    , ServiceRoute.child "Input" "gui_fb"
                    , ServiceRoute.parent "File_system"
                    ]
                  , config = Init.Config::{
                    , content =
                      [ XML.element
                          { name = "palette"
                          , attributes = toMap
                              { uri = "https://pippin.gimp.org/ametameric/" }
                          , content =
                              let color =
                                    λ(index : Natural) →
                                    λ(value : Text) →
                                      XML.leaf
                                        { name = "color"
                                        , attributes = toMap
                                            { index = Natural/show index
                                            , value
                                            }
                                        }

                              in  [ color 0 "#000000"
                                  , color 1 "#a02929"
                                  , color 2 "#4aa08b"
                                  , color 3 "#878453"
                                  , color 4 "#2424ed"
                                  , color 5 "#ab4adf"
                                  , color 6 "#3b6bb1"
                                  , color 7 "#c3c3c3"
                                  , color 8 "#6f6f6f"
                                  , color 9 "#edac82"
                                  , color 10 "#99edba"
                                  , color 11 "#e9d808"
                                  , color 12 "#82b4ed"
                                  , color 13 "#d66fed"
                                  , color 14 "#1de1ed"
                                  , color 15 "#ffffff"
                                  ]
                          }
                      , VFS.vfs
                          [ VFS.leafAttrs
                              "rom"
                              (toMap { name = params.fontFile })
                          , VFS.dir
                              "fonts"
                              [ VFS.dir
                                  "monospace"
                                  [ VFS.leafAttrs
                                      "ttf"
                                      ( toMap
                                          { name = "regular"
                                          , path = params.fontFile
                                          , size_px = "10"
                                          }
                                      )
                                  ]
                              ]
                          ]
                      ]
                    , policies =
                      [ Init.Config.Policy::{
                        , service = "Terminal"
                        , label = Init.LabelSelector.prefix "terminal_log"
                        }
                      ]
                    }
                  }
            , terminal_log =
                Child.flat
                  Child.Attributes::{
                  , binary = "terminal_log"
                  , config = Init.Config::{
                    , policies =
                      [ Init.Config.Policy::{
                        , service = "LOG"
                        , label = Init.LabelSelector.prefix "core"
                        }
                      , Init.Config.Policy::{
                        , service = "LOG"
                        , label = Init.LabelSelector.prefix "kernel"
                        }
                      ]
                    }
                  }
            , core =
                Child.flat
                  Child.Attributes::{
                  , binary = "log_core"
                  , routes = [ routeLogRom "core_log" ]
                  }
            , kernel =
                Child.flat
                  Child.Attributes::{
                  , binary = "log_core"
                  , routes = [ routeLogRom "kernel_log" ]
                  }
            }
        }
        Init.Attributes.default

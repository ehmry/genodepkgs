{ config, pkgs, lib, ... }:

with lib;

let cfg = config.genode.gui;
in {
  options.genode.gui = {
    enable = mkEnableOption "Genode Gui service";
    consoleLog = { enable = mkEnableOption "console log"; };
  };

  config = {

    genode.gui.enable = cfg.consoleLog.enable;

    hardware.genode.framebuffer.enable = cfg.enable;

    genode.core.children.nitpicker = mkIf cfg.enable {
      inputs = [ pkgs.genodePackages.nitpicker ];
      configFile = pkgs.writeText "nitpicker.dhall" ''
        let Init = (env:DHALL_GENODE).Init

        in  ${./nitpicker.dhall}
              { policies =
                [ Init.Config.Policy::{
                  , service = "Gui"
                  , label = Init.LabelSelector.prefix "consoleLog"
                  , attributes = toMap { domain = "default" }
                  }
                ]
              }
      '';
    };

    genode.boot.romModules = mkIf cfg.consoleLog.enable {
      "TerminusTTF.ttf" = pkgs.buildPackages.terminus_font_ttf
        + "/share/fonts/truetype/TerminusTTF.ttf";
    };

    genode.core.children.consoleLog = mkIf cfg.consoleLog.enable {
      inputs = with pkgs.genodePackages; [
        gui_fb
        log_core
        libc
        terminal
        terminal_log
        vfs_ttf
      ];
      coreROMs = [ "core_log" "kernel_log" ];
      configFile = pkgs.writeText "consoleLog.dhall" ''
        ${./consoleLog.dhall} { fontFile = "TerminusTTF.ttf" }
      '';
    };

  };
}

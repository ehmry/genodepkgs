{ genodePackages }:

let
  inherit (genodePackages) genodeSources;
  port = genodeSources.ports.stdcxx;
in genodeSources.buildUpstream {
  name = "stdcxx";
  outputs = [ "out" "dev" ];
  targets = [ "LIB=stdcxx" ];
  portInputs = [ genodeSources.ports.libc port ];
  propagatedBuildInputs = [ genodeSources.genodeBase ];

  STDCXX_PORT = port;
  postInstall = ''
    mkdir -p $dev/include

    pushd $STDCXX_PORT/*
    cp -r --no-preserve=mode \
      include/stdcxx/* \
      $GENODE_DIR/repos/libports/include/stdcxx/* \
      $dev/include/
    popd
  '';
}

{ stdenv, fetchgit, dhallPackages }:

dhallPackages.buildDhallDirectoryPackage {
  name = "dhall-genode";
  src = fetchgit {
    url = "https://git.sr.ht/~ehmry/dhall-genode";
    rev = "eb3eba6e4719f0e4589e92a490a4a1c45511d58d";
    sha256 = "0xh4bndx5fncdkc8jbm64c7hqsyhglw5xjkgff0wfvycw9ldyg7x";
  };
  dependencies = [ dhallPackages.Prelude ];
}

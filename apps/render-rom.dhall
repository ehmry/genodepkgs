
let Genode = env:DHALL_GENODE

let render =
        λ(boot : Genode.Boot.Type)
      → let rom =
                Genode.BootModules.toRomTexts
                  (toMap { config = Genode.Init.render boot.config })
              # boot.rom

        in  rom

in  render

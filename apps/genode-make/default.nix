{ writeScriptBin, runtimeShell }:

writeScriptBin "genode-make" ''
  #!${runtimeShell}
  exec nix build -f ${./make.nix} --argstr target $@
'' + "/bin/genode-make"

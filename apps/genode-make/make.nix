{ target }:
let
  flake = getFlake "genodepkgs";
  pkgs = flake.packages."${builtins.currentSystem}-x86_64-genode";
in pkgs.genodeSources.make target

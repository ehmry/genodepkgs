
let Genode = env:DHALL_GENODE

let toRom =
        λ(mapKey : Text)
      → λ(path : Text)
      → [ { mapKey = mapKey
          , mapValue = Genode.BootModules.ROM.Type.RomPath path
          }
        ]

in  toRom

{ system, self, nixpkgs, nixpkgsLocal, packages }:

let
  dhallApps = let
    mkApp = { drv, name ? drv.pname or drv.name, exe ? name }: {
      inherit name;
      value = {
        type = "app";
        program = "${drv}/bin/${exe}";
      };
    };
  in builtins.listToAttrs [
    (mkApp { drv = nixpkgsLocal.dhall; })

    (mkApp {
      drv = nixpkgsLocal.dhall-bash;
      name = "dhall-to-bash";
    })

    (mkApp {
      drv = nixpkgsLocal.dhall-json;
      name = "dhall-to-json";
    })

    (mkApp {
      drv = nixpkgsLocal.dhall-json;
      name = "json-to-dhall";
    })

    (mkApp {
      drv = nixpkgsLocal.dhall-json;
      name = "dhall-to-yaml";
    })

    (mkApp {
      drv = nixpkgsLocal.dhall-json;
      name = "yaml-to-dhall";
    })

  ];

in dhallApps // {

  genode-make = {
    type = "app";
    program = nixpkgs.buildPackages.callPackage ./genode-make { };
  };

  linux-image = let
    drv = import ./linux-image {
      apps = self;
      inherit nixpkgs packages;
    };
  in {
    type = "app";
    program = "${drv}/bin/linux-image";
  };

  generate-manifest = let
    drv = import ./generate-manifest {
      stdenv = packages.stdenv;
      inherit nixpkgs;
    };
  in {
    type = "app";
    program = "${drv}/bin/generate-manifest";
  };

  render-init = let
    drv = import ./render-init {
      apps = self;
      inherit nixpkgs packages;
    };
  in {
    type = "app";
    program = "${drv}/bin/render-init";
  };

} // (if system == "x86_64-genode" then {

  hw-image = let
    drv = import ./hw-image {
      stdenv = packages.stdenv;
      apps = self;
      inherit nixpkgs packages;
    };
  in {
    type = "app";
    program = "${drv}/bin/hw-image";
    function = attrs: bootDesc:
      nixpkgs.runCommand "image.elf" attrs
      ''XDG_CACHE_HOME=$TMPDIR ${drv}/bin/hw-image "${bootDesc}" > $out'';
  };

  hw-iso = let
    drv = import ./hw-iso {
      stdenv = packages.stdenv;
      inherit nixpkgs packages;
      inherit (self) hw-image;
    };
  in {
    type = "app";
    program = "${drv}/bin/hw-iso";
    function = attrs: bootDesc:
      nixpkgs.runCommand "hw.iso" attrs
      ''XDG_CACHE_HOME=$TMPDIR ${drv}/bin/hw-iso "${bootDesc}" > $out'';
  };

  nova-image = let
    drv = import ./nova-image {
      stdenv = packages.stdenv;
      apps = self;
      inherit nixpkgs packages;
    };
  in {
    type = "app";
    program = "${drv}/bin/nova-image";
  };

  nova-iso = let
    drv = import ./nova-iso {
      stdenv = packages.stdenv;
      inherit nixpkgs packages;
      inherit (self) nova-image;
    };
  in {
    type = "app";
    program = "${drv}/bin/nova-iso";
    function = attrs: bootDesc:
      nixpkgs.runCommand "nova.iso" attrs
      ''XDG_CACHE_HOME=$TMPDIR ${drv}/bin/nova-iso "${bootDesc}" > $out'';
  };

} else
  { })

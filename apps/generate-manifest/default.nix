{ stdenv, nixpkgs }:

with nixpkgs.buildPackages;

writeScriptBin "generate-manifest" ''
  #!${runtimeShell}
  root="$1"
  if [ -z "$root" ]; then
    echo a directory must be passed as an argument >> /dev/stderr
    exit 1
  fi
  if [ ! -d "$root" ]; then
    echo \'$root\' is not an existing directory >> /dev/stderr
    exit 1
  fi

  set -eu
  echo -n '['
  find $root/ -type f -printf ',{mapKey= "%f",mapValue="%p"}'
  echo -n ']'
''

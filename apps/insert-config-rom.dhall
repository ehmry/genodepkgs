
let Genode = env:DHALL_GENODE

let BootModules = Genode.BootModules

in    λ(configPath : Text)
    → λ(rom : Genode.BootModules.Type)
    → BootModules.toRomPaths (toMap { config = configPath }) # rom

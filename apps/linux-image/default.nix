{ nixpkgs, apps, packages }:

nixpkgs.writeScriptBin "linux-image" (with nixpkgs.buildPackages; ''
  #!${runtimeShell}
  set -eu
  export DHALL_GENODE=''${DHALL_GENODE:-${packages.dhallGenode}/source.dhall}
  ${apps.dhall.program} text <<< "${./script.dhall} ($@)" > boot.sh
  source boot.sh
  rm boot.sh
'')

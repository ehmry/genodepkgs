{ nixpkgs, apps, packages }:

nixpkgs.writeScriptBin "render-init" (with nixpkgs.buildPackages; ''
  #!${runtimeShell}
  set -eu
  export DHALL_GENODE=''${DHALL_GENODE:-${packages.dhallGenode}/source.dhall}
  ${apps.dhall.program} text \
    <<< "(env:DHALL_GENODE).Init.render ($(cat))" \
    | ${nixpkgs.buildPackages.libxml2}/bin/xmllint -format - \
    | sed 's/&gt;/>/g'
'')

{ stdenv, nixpkgs, apps, packages }:

let inherit (packages) genodeSources base-nova;
in nixpkgs.writeScriptBin "nova-image" (with nixpkgs.buildPackages;
  let inherit (stdenv) cc;

  in ''
    #!${runtimeShell}
    set -eu

    CC="${cc}/bin/${cc.targetPrefix}cc"
    LD="${buildPackages.binutils}/bin/${buildPackages.binutils.targetPrefix}ld"

    TMPDIR="$(${coreutils}/bin/mktemp -d)"
    trap "rm -rf $TMPDIR" err exit

    export DHALL_GENODE=${packages.dhallGenode}/source.dhall

    ${apps.dhall.program} text \
      <<< "(env:DHALL_GENODE).Init.render ($@).config" \
      > $TMPDIR/config

    ${apps.dhall.program} text \
      <<< "(${../modules.as.dhall}).to64bitImage (${
        ../insert-config-rom.dhall
      } \"$TMPDIR/config\" ($@).rom)" \
      > "$TMPDIR/modules.as"

    # compile the boot modules into one object file
    $CC -c -x assembler -o "$TMPDIR/boot_modules.o" "$TMPDIR/modules.as"

    # link final image
    $LD --strip-all \
    	-T${genodeSources}/repos/base/src/ld/genode.ld \
    	-T${genodeSources}/repos/base-nova/src/core/core-bss.ld \
    	-z max-page-size=0x1000 \
    	-Ttext=0x100000 -gc-sections \
    	"''${CORE_OBJ:-${base-nova.coreObj}}" "$TMPDIR/boot_modules.o"
    cat a.out
  '')

{ stdenv, nixpkgs, packages, nova-image }:

nixpkgs.writeScriptBin "nova-iso" (with nixpkgs.buildPackages;
  let inherit (stdenv) cc;

  in ''
    #!${runtimeShell}
    set -eu

    SYSLINUX="${syslinux}/share/syslinux"

    TMPDIR="$(${coreutils}/bin/mktemp -d)"
    trap "rm -rf $TMPDIR" err exit

    mkdir -p "$TMPDIR/boot/syslinux"
    ${nova-image.program} $@ > "$TMPDIR/boot/image.elf"

    pushd "$TMPDIR" > /dev/null
    out="nova.iso"

    # build ISO layout
    cp ${packages.NOVA}/hypervisor* boot/hypervisor
    cp ${./isolinux.cfg} boot/syslinux/isolinux.cfg
    cp \
    	$SYSLINUX/isolinux.bin \
    	$SYSLINUX/ldlinux.c32 \
    	$SYSLINUX/libcom32.c32 \
    	$SYSLINUX/mboot.c32 \
    	boot/syslinux
    chmod +w boot/syslinux/isolinux.bin

    # create ISO image
    ${cdrkit}/bin/mkisofs -o "$out" \
    	-b syslinux/isolinux.bin -c syslinux/boot.cat \
    	-no-emul-boot -boot-load-size 4 -boot-info-table \
    	-iso-level 2 \
    	boot

    ${syslinux}/bin/isohybrid "$out"
    cat "$out"
  '')

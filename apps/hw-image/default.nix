{ stdenv, nixpkgs, apps, packages }:

let inherit (packages) genodeSources base-hw-pc;
in nixpkgs.writeScriptBin "hw-image" (with nixpkgs.buildPackages;
  let inherit (stdenv) cc;

  in ''
    #!${runtimeShell}
    set -eu

    CC="${cc}/bin/${cc.targetPrefix}cc"
    LD="${buildPackages.binutils}/bin/${buildPackages.binutils.targetPrefix}ld"

    TMPDIR="$(${coreutils}/bin/mktemp -d)"
    trap "rm -rf $TMPDIR" err exit

    export DHALL_GENODE=''${DHALL_GENODE:-${packages.dhallGenode}/binary.dhall}
    if [ -z "$XDG_CACHE_HOME" ]
    then
      export XDG_CACHE_HOME="$TMPDIR"
      ${buildPackages.xorg.lndir}/bin/lndir -silent \
        "${packages.dhallGenode}/.cache" \
        "$XDG_CACHE_HOME"
    fi

    build_core() {
      local lib="$1"
      local modules="$2"
      local link_address="$3"
      ${apps.dhall.program} text > "$TMPDIR/modules.as" \
         <<< "(${../modules.as.dhall}).to64bitImage ($modules)" \

      # compile the boot modules into one object file
      $CC -c -x assembler -o "$TMPDIR/boot_modules.o" "$TMPDIR/modules.as"

      # link final image
      $LD \
        --strip-all \
        -T${genodeSources}/repos/base/src/ld/genode.ld \
        -z max-page-size=0x1000 \
        -Ttext=$link_address -gc-sections \
        "$lib" "$TMPDIR/boot_modules.o"
        cat a.out
    }

    ${apps.dhall.program} text \
      <<< "(env:DHALL_GENODE).Init.render ($@).config" \
      > $TMPDIR/config

    ${apps.dhall.program} \
      <<< "${../insert-config-rom.dhall} \"$TMPDIR/config\" ($@).rom" \
      > "$TMPDIR/modules.dhall"

    build_core "''${CORE_OBJ:-${base-hw-pc.coreObj}}" "$TMPDIR/modules.dhall" 0xffffffc000000000 > core.elf
    build_core "''${BOOTSTRAP_OBJ:-${base-hw-pc.bootstrapObj}}" "${
      ../to-rom.dhall
    } \"core.elf\" \"./core.elf\"" 0x00200000
  '')

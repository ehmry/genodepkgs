{
  description = "Genode packages";

  inputs.nixpkgs.url = "github:ehmry/nixpkgs/genodepkgs";

  outputs = { self, nixpkgs }:
    let
      localSystems = [ "x86_64-linux" ];
      crossSystems = [ "aarch64-genode" "x86_64-genode" ];

      forAllLocalSystems = f:
        nixpkgs.lib.genAttrs localSystems (system: f system);

      forAllCrossSystems = f:
        with builtins;
        let
          f' = localSystem: crossSystem:
            let system = localSystem + "-" + crossSystem;
            in {
              name = system;
              value = f { inherit system localSystem crossSystem; };
            };
          list = nixpkgs.lib.lists.crossLists f' [ localSystems crossSystems ];
          attrSet = listToAttrs list;
        in attrSet;

      forAllSystems = f:
        (forAllCrossSystems f) // (forAllLocalSystems (system:
          f {
            inherit system;
            localSystem = system;
            crossSystem = system;
          }));

      nixpkgsFor = forAllSystems ({ system, localSystem, crossSystem }:
        if localSystem == crossSystem then
          import nixpkgs {
            inherit system;
            overlays = [ self.overlay ];
          }
        else
          import nixpkgs {
            inherit localSystem;
            crossSystem = {
              system = crossSystem;
              useLLVM = true;
            };
            config.allowUnsupportedSystem = true;
            overlays = [ self.overlay ];
          });

    in rec {

      overlay =
        # Overlay of fixes applied to Nixpkgs
        import ./overlay { flake = self; };

      lib =
        # Local utilities merged with the Nixpkgs lib
        forAllCrossSystems ({ system, localSystem, crossSystem }:
          nixpkgs.lib // (import ./lib {
            inherit system localSystem crossSystem;
            pkgs = self.legacyPackages.${system};
          })) // {
            inherit forAllSystems forAllLocalSystems forAllCrossSystems;
          };

      legacyPackages =
        # The nixpkgs.legacyPackages after overlaying
        # and with some additional Genode packages
        forAllSystems
        ({ system, localSystem, crossSystem }: nixpkgsFor.${system});
      # pass thru Nixpkgs

      packages =
        # Genode native packages, not packages in the traditional
        # sense in that these cannot be installed within a profile
        forAllCrossSystems ({ system, localSystem, crossSystem }:
          nixpkgs.lib.filterAttrs (n: v: v != null)
          nixpkgsFor.${system}.genodePackages);

      devShell =
        # Development shell for working with the
        # upstream Genode source repositories
        forAllLocalSystems (system:
          let
            pkgs = nixpkgsFor.${system};
            fhs = pkgs.buildFHSUserEnv {
              name = "genode-env";
              targetPkgs = pkgs:
                (with pkgs; [
                  binutils
                  bison
                  expect
                  flex
                  git
                  glibc.dev
                  gnumake
                  libxml2
                  qemu
                  tcl
                  which
                  xorriso
                ]);
              runScript = "bash";
              extraBuildCommands = let
                toolchain = pkgs.fetchzip {
                  url =
                    "file://${packages.x86_64-linux-x86_64-genode.genodeSources.toolchain.src}";
                  hash = "sha256-26rPvLUPEJm40zLSqTquwuFTJ1idTB0T4VXgaHRN+4o=";
                };
              in "ln -s ${toolchain}/local usr/local";
            };
          in pkgs.stdenv.mkDerivation {
            name = "genode-fhs-shell";
            nativeBuildInputs = [ fhs ];
            shellHook = "exec genode-env";
          });

      apps =
        # Utilities accessible via "nix run"
        forAllCrossSystems ({ system, localSystem, crossSystem }:
          import ./apps {
            inherit system;
            self = self.apps.${system};
            nixpkgs = nixpkgsFor.${system};
            nixpkgsLocal = nixpkgsFor.${localSystem};
            packages = self.packages.${system};
          });

      nixosModules =
        # Modules for composing Genode and NixOS
        import ./nixos-modules { flake = self; };

      checks =
        # Checks for continous testing
        let tests = import ./tests;
        in with (forAllCrossSystems ({ system, localSystem, crossSystem }:
          tests {
            flake = self;
            inherit system localSystem crossSystem;
            pkgs = self.legacyPackages.${system};
          } // {
            ports = nixpkgsFor.${localSystem}.symlinkJoin {
              name = "ports";
              paths = (builtins.attrValues
                self.packages.${system}.genodeSources.ports);
            };
          })); {
            x86_64-linux = x86_64-linux-aarch64-genode
              // x86_64-linux-x86_64-genode;
          };

      hydraJobs = self.checks;

    };
}
